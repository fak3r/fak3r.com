https://www.nytimes.com/2017/07/08/opinion/sunday/aurora-movie-theater-shooting-colorado.html?smid=tw-nytopinion&smtyp=cur


By one accounting, an average of 315 people are shot every day in this country. While 93 of them die in murders, assaults, suicide attempts and gun accidents, there are 222 who were shot and survive with various degrees of trauma, much of it lifelong, according to a five-year survey of federal data by the Brady Campaign to Prevent Gun Violence.

http://www.bradycampaign.org/key-gun-violence-statistics
