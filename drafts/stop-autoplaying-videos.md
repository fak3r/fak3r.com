

https://www.consumerreports.org/internet/how-to-stop-videos-from-autoplaying/

The videos can be particularly annoying when you have several tabs open and can’t tell where the sound is coming from.

Blocking these videos not only cuts down on the distraction but can also keep you from needlessly burning through cellular data. That's handy if you’ve tethered your laptop to a smartphone at a café or have an internet service provider (ISP) with a monthly data cap.

And using a browser extension to block annoying autoplay videos has the added benefit of enhancing your overall online privacy. Those videos often come packaged with technology that can track your activity across the web for the benefit of advertisers.

“Searches about your health condition, your political engagement—you probably want to keep those secret,” and blocking autoplay videos can help, says Günes Acar, a research associate at Princeton University’s Center for Information Technology Policy. 
Browser Settings

Chrome: There’s good and bad news when it comes to disabling autoplay videos in Chrome, the most popular desktop web browser on the planet, according to the analysis firm StatCounter. The good news is that Google is planning a version of Chrome that will block autoplaying video right out of the box, but the bad news is that the company doesn’t have a firm release date. In the meantime you can use a third-party extension to stop the videos. (See the Extensions section below.)

Firefox: Mozilla’s recently revamped Firefox web browser offers a straightforward way to prevent unwanted videos from playing automatically as you hop from site to site. It's straightforward, but not something you'd stumble across by searching through pull-down menus.

First, you’ll need to access the browser’s advanced options by typing “about:config” (without the quotation marks) in the address bar and hitting Return. Next, search in the window for the “media.autoplay.enabled” setting and double-click it. This will disable most HTML5-based videos from playing on their own when you load a page.

This method won’t typically block videos in the older Adobe Flash format, but we’ll explain how to do so below.

Safari: If you use Apple’s Safari browser as your daily driver, you may be wondering to yourself, “What’s autoplay video?” That’s because in the newest versions of Safari (starting with version 11, released in 2017), most autoplaying videos are blocked. Score one for Apple.

If you don’t have the latest version of Safari—say, if you’re using a work-provided laptop that has an older version of macOS—it’s a bit trickier to pull off.

You’ll need to open the application called Terminal (which is found in your Utilities folder), then paste in the following text (without the quotation marks): “defaults write com.apple.Safari IncludeInternalDebugMenu 1”.

This scary-looking string merely allows you to access an additional settings menu inside Safari called Debug. The next time you go to a website that has autoplaying video, you have to select the new Debug menu option at the top of the screen and then scroll down to Media Flags and Disable Inline Video. Goodbye, autoplaying video!
Extensions

As mentioned above, there's no built-in setting for Chrome that lets you turn off autoplay for videos, but you can do it by using a browser extension.

Regardless of which browser you use, extensions (also known as add-ons) are small pieces of software that add useful features. Some extensions, such as LastPass, help manage your passwords, while others, including the Evernote Web Clipper and OneNote Web Clipper, make it easier to archive snapshots of websites for later retrieval.

Some of the most popular extensions block ads from appearing on websites, with two, Adblock Plus and uBlock Origin, generally earning high reviews from users.

It just so happens that both Adblock Plus and uBlock Origin will block most autoplaying videos, including those based on the older Adobe Flash, right after you install them. No further configuration is needed.

Another extension, called NoScript, goes even further, and is able to scrub out entire sections of websites, including ads, videos, or blocks of text. The flip side of NoScript is that it’s generally more difficult to configure than either Adblock Plus or uBlock Origin, and can easily keep many websites from working for you if you accidentally configure it to block a vital script.
Facebook

With more than 2 billion active users, there’s a good chance that Facebook has an account for everyone reading this sentence. And while the company has recently announced changes to the News Feed that should limit the amount of video you see from brands and businesses, it still does allow autoplay videos. But Facebook also provides a setting that disables videos from playing automatically in your feed. 

To use this setting, click the arrow icon on the top right-hand corner of your Facebook page and then click Settings. From here, select Videos on the left side of the window and change the Auto-Play Videos option from Default to Off.
