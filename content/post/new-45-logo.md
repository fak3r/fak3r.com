+++
linktitle = "New 45 logo"
featuredalt = ""
categories = [ "politics" ]
description = "Events in Charlottesville changed the equation, this will not stand"
title = "New 45 logo"
featured = ""
featuredpath = ""
author = "fak3r"
date = "2017-08-15T20:40:21-06:00"

+++

Thanks to Mike Mitchell [@sirmitchell](https://twitter.com/sirmitchell) for the new 45 logo, inspired by Trumps recent comments on the Charlottesville clash.
<div align="center">
<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Here&#39;s a high res copy which I&#39;m allowing for personal use (signs, shirts, buttons). Spread it far and wide: <a href="https://t.co/k0GqGslx6W">https://t.co/k0GqGslx6W</a> 🚫45 <a href="https://t.co/1bWM00CHtj">pic.twitter.com/1bWM00CHtj</a></p>&mdash; Mike Mitchell (@sirmitchell) <a href="https://twitter.com/sirmitchell/status/897266210563383296">August 15, 2017</a></blockquote>
</div>
<br>
> "When someone shows you who they are, you should believe them. And Donald Trump is again letting Nazis and white supremacists off the hook for their violence in Charlottesville. This is not acceptable behavior for any American. This is especially appalling coming from the President of the United States." ~ U.S. Senator Al Franken [<a href="https://www.facebook.com/senatoralfranken/posts/1465924120160132?pnref=story">source</a>]
<br>
> "America must always reject racial bigotry, anti-Semitism, and hatred in all forms. As we pray for Charlottesville, we are reminded of the fundamental truths recorded by that city's most prominent citizen in the Declaration of Independence: we are all created equal and endowed by our Creator with unalienable rights. We know these truths to be everlasting because we have seen the decency and greatness of our country." ~ Joint statement from former president George H.W. Bush and his son former president George W. Bush [<a href="https://www.washingtonpost.com/news/politics/wp/2017/08/16/both-bush-presidents-just-spoke-out-on-charlottesville-and-sound-nothing-like-trump/">source</a>]
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
